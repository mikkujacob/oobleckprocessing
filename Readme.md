#Oobleck Processing

##Project Concept
This is a Java repository containing source code for the second mini project of the Fall 2016 Digital Media Prototyping course at Georgia Tech with the theme "joy".
This project controls a living installation containing a pool of Oobleck vibrated by a sub-woofer as a physical realization of a participant's emotional confession.

##Instructions
1. Import into Eclipse as a Java project.
2. Run the OobleckApplication.java file as a Java Application.

##Materials
1. Oobleck (2 parts cornstarch to 1 part water)
2. Sub-woofer with speaker cone exposed
3. Computer
4. Microphone
