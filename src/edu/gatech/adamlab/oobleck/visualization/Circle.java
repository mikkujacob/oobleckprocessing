package edu.gatech.adamlab.oobleck.visualization;

import processing.core.PApplet;
import processing.core.PVector;

public class Circle
{
	private PApplet parent;
	private PVector origin;
	private float radius;
	private int color;
	private long expirationTimestamp;
	private boolean isRadialCoordinates = false;
	
	public Circle (PApplet parent, PVector origin, float radius)
	{
		this(parent, new PVector(), radius, parent.color(255), System.currentTimeMillis());
	}
	
	public Circle(PApplet parent, PVector origin, float radius, int color)
	{
		this(parent, origin, radius, color, System.currentTimeMillis());
	}
	
	public Circle(PApplet parent, PVector origin, float radius, long expirationTimestamp)
	{
		this(parent, origin, radius, parent.color(255), expirationTimestamp);
	}
	
	public Circle(PApplet parent, PVector origin, float radius, int color, long expirationTimestamp)
	{
		this.parent = parent;
		this.origin = new PVector(origin.x, origin.y, origin.z);
		this.radius = radius;
		this.color = color;
		this.expirationTimestamp = expirationTimestamp;
	}
	
	public void draw()
	{
		int oldFill = parent.g.fillColor;
		parent.noStroke();
		parent.fill(color);
		parent.pushMatrix();
		if(isRadialCoordinates)
		{
			parent.translate(parent.displayWidth / 2f, parent.displayHeight / 2f);
			parent.rotate(PApplet.map(origin.x, 0, parent.displayWidth, 0, 2 * PApplet.PI));
			float height = PApplet.map(origin.y, 0, parent.displayHeight, radius / 4f, parent.displayHeight / 2);
			parent.ellipse(0, height, radius, radius);
		}
		else
		{
			parent.ellipse(origin.x, origin.y, radius, radius);
		}
		parent.fill(oldFill);
		parent.popMatrix();
	}

	/**
	 * @return the origin
	 */
	public PVector getOrigin()
	{
		return origin;
	}

	/**
	 * @param origin the origin to set
	 */
	public void setOrigin(PVector origin)
	{
		this.origin = origin;
	}

	/**
	 * @return the radius
	 */
	public float getRadius()
	{
		return radius;
	}

	/**
	 * @param radius the radius to set
	 */
	public void setRadius(float radius)
	{
		this.radius = radius;
	}

	/**
	 * @return the color
	 */
	public int getColor()
	{
		return color;
	}

	/**
	 * @param color the color to set
	 */
	public void setColor(int color)
	{
		this.color = color;
	}

	/**
	 * @return the expirationTimestamp
	 */
	public long getExpirationTimestamp()
	{
		return expirationTimestamp;
	}

	/**
	 * @param expirationTimestamp the expirationTimestamp to set
	 */
	public void setExpirationTimestamp(long expirationTimestamp)
	{
		this.expirationTimestamp = expirationTimestamp;
	}

	public boolean isRadialCoordinates()
	{
		return isRadialCoordinates;
	}

	public void setRadialCoordinates(boolean isRadialCoordinates)
	{
		this.isRadialCoordinates = isRadialCoordinates;
	}
}
