package edu.gatech.adamlab.oobleck.visualization;

import java.util.ArrayList;
import java.util.Random;

import com.thomasdiewald.pixelflow.java.DwPixelFlow;
import com.thomasdiewald.pixelflow.java.fluid.DwFluid2D;
import com.thomasdiewald.pixelflow.java.fluid.DwFluid2D.FluidData;

import blobDetection.Blob;
import blobDetection.BlobDetection;
import blobDetection.EdgeVertex;
import ddf.minim.analysis.FFT;
import edu.gatech.adamlab.oobleck.audio.analysis.SoundAnalysis;
import edu.gatech.adamlab.oobleck.depth.KinectInterface;
import processing.core.PApplet;
import processing.core.PFont;
import processing.core.PImage;
import processing.core.PVector;
import processing.opengl.PGraphics2D;

public class Visualization
{
	private VisualizationType visualizationType;
	
	private PApplet parent;
	private SoundAnalysis analysis;
	private ColorSpaces colorSpaces;
	private Random random;
	private ArrayList<Circle> circles;
	private boolean isRadialCoordinates;
	private PFont font;
	private KinectInterface kinect;
	private DwPixelFlow context;
	private DwFluid2D fluid;
	private PGraphics2D pg_fluid;
	private PGraphics2D pg_obstacles;
	private PGraphics2D pg_blobs;
	private boolean isBlobDetectionActive;
	private BlobDetection blobDetection;
	private ArrayList<Circle> blobs;
	private int maxBlobs = 30;

//	private float spectrumScale = 5;
	private float threshold = 10f;
	private int fluidGridScale = 4;
	
	public Visualization(PApplet parent, SoundAnalysis analysis, KinectInterface kinect)
	{
		this.parent = parent;
		this.analysis = analysis;
		this.kinect = kinect;
		
		circles = new ArrayList<Circle>();
		colorSpaces = new ColorSpaces(parent);
		random = new Random();
		font = parent.loadFont("ArialMT-12.vlw");
		
		setVisualizationType(VisualizationType.BOKEH);
		context = new DwPixelFlow(parent);
		context.printGL();
		
		initializeFluidSimulation();
		
		isBlobDetectionActive = true;
		blobDetection = new BlobDetection(parent.width, parent.height);
		blobDetection.setPosDiscrimination(true);
		blobDetection.setThreshold(0.8f);
		blobs = new ArrayList<Circle>();
		pg_blobs = (PGraphics2D) parent.createGraphics(parent.width, parent.height, PApplet.P2D);

		parent.textFont(font);
		parent.textSize(18);
	}
	
	private void initializeFluidSimulation()
	{
		fluid = new DwFluid2D(context, parent.width, parent.height, fluidGridScale);
		fluid.param.dissipation_density     = 0.9f;
	    fluid.param.dissipation_velocity    = 0.9f;
	    fluid.param.dissipation_temperature = 0.7f;
	    fluid.param.vorticity               = 0.10f;
	    
	    fluid.addCallback_FluiData(new FluidData()
		{
			
			@Override
			public void update(DwFluid2D fluid)
			{
				//ADD MONITORING OF THE OOBLECK AND ADDING IMPULSES TO THE FLUID
				float px, py, vx, vy, radius, vscale, r, g, b, intensity;//, temperature;
				
				if(parent.mousePressed)
				{
					// add impulse: density + velocity
					if(parent.mouseButton == PApplet.LEFT)
					{
						radius = 15;
						vscale = 15;
					    r = 1.0f;
					    g = 0.0f;
					    b = 0.0f;
					    intensity = 1.0f;
					    px     = parent.mouseX;
					    py     = parent.height - parent.mouseY;
					    vx     = (parent.mouseX - parent.pmouseX) * + vscale;
					    vy     = (parent.mouseY - parent.pmouseY) * - vscale;
					    
					    fluid.addDensity(px, py, radius, r, g, b, intensity);
					    fluid.addVelocity(px, py, radius, vx, vy);
					}
					
					// add impulse: density + velocity
					if(parent.mouseButton == PApplet.CENTER)
					{
						radius = 15;
						vscale = 15;
					    r = 0.0f;
					    g = 1.0f;
					    b = 0.0f;
					    intensity = 1.0f;
					    px     = parent.mouseX;
					    py     = parent.height - parent.mouseY;
					    vx     = (parent.mouseX - parent.pmouseX) * + vscale;
					    vy     = (parent.mouseY - parent.pmouseY) * - vscale;
					    
					    fluid.addDensity(px, py, radius, r, g, b, intensity);
					    fluid.addVelocity(px, py, radius, vx, vy);
					}
					
					// add impulse: density + velocity
					if(parent.mouseButton == PApplet.RIGHT)
					{
						radius = 15;
						vscale = 15;
					    r = 0.0f;
					    g = 0.0f;
					    b = 1.0f;
					    intensity = 1.0f;
					    px     = parent.mouseX;
					    py     = parent.height - parent.mouseY;
					    vx     = (parent.mouseX - parent.pmouseX) * + vscale;
					    vy     = (parent.mouseY - parent.pmouseY) * - vscale;
					    
					    fluid.addDensity(px, py, radius, r, g, b, intensity);
					    fluid.addVelocity(px, py, radius, vx, vy);
					}
				}
				
				Circle circle = null;
				for(int i = 0; i < blobs.size(); i++)
				{
					circle = blobs.get(i);
					radius = 1 + random.nextInt(15);;
					vscale = 1 + random.nextInt(25);
				    r = parent.red(circle.getColor()) / 255f;
				    g = parent.green(circle.getColor()) / 255f;
				    b = parent.blue(circle.getColor()) / 255f;
				    intensity = 1.0f;
				    px     = circle.getOrigin().x;
				    py     = circle.getOrigin().y;
				    vx     = (random.nextBoolean() ? 1 : -1) * vscale;
				    vy     = (random.nextBoolean() ? 1 : -1) * vscale;
				    
				    fluid.addDensity(px, py, radius, r, g, b, intensity);
				    fluid.addVelocity(px, py, radius, vx, vy);
				}
			}
		});
	    
	    pg_fluid = (PGraphics2D) parent.createGraphics(parent.width, parent.height, PApplet.P2D);
	    pg_fluid.smooth(4);
	    pg_fluid.beginDraw();
	    pg_fluid.background(0);
	    pg_fluid.endDraw();
	    
	    pg_obstacles = (PGraphics2D) parent.createGraphics(parent.width, parent.height, PApplet.P2D);
	    pg_obstacles.noSmooth();
	    pg_obstacles.beginDraw();
	    pg_obstacles.background(0);
	    pg_obstacles.clear();
	    //Boundary circle obstacle
	    float radius = parent.height;
	    pg_obstacles.stroke(parent.color(255));
	    pg_obstacles.strokeWeight(20);
	    pg_obstacles.noFill();
	    pg_obstacles.ellipse(parent.width / 2f, parent.height / 2f, radius - pg_obstacles.strokeWeight, radius - pg_obstacles.strokeWeight);
	    pg_obstacles.endDraw();
	    
	    fluid.addObstacles(pg_obstacles);
	}
	
	public void draw()
	{
		kinect.update();
		updateFluid();
		
		if(visualizationType == VisualizationType.FLUID)
		{
			drawFluid();
			if(isBlobDetectionActive)
			{
				drawBlobs();
			}
		}
		else
		{
			drawBokeh();
		}
	}
	
	private void updateFluid()
	{
//		fluid.addObstacles(pg_obstacles);
	    fluid.update();
	}
	
	private void drawBlobs()
	{
		pg_blobs.beginDraw();
		pg_blobs.background(0);
		pg_blobs.image(pg_obstacles, 0, 0);
		pg_blobs.endDraw();
		
		pg_blobs.loadPixels();
		blobDetection.computeBlobs(pg_blobs.pixels);
		
//		pg_obstacles.loadPixels();
//		blobDetection.computeBlobs(pg_obstacles.pixels);
		
		Blob blob = null;
		Circle circle = null;
		int oldFill = parent.g.fillColor;
//		System.out.println("Blob Number: " + blobDetection.getBlobNb());
		for(int i = 0; i < blobDetection.getBlobNb(); i++)
		{
			blob = blobDetection.getBlob(i);
			if(blob != null && i > 8 && blob.w * parent.width > parent.width / 12 && blob.h * parent.height > parent.height / 12)
//			if(blob != null)
			{
				float radius = (blob.w < blob.h) ? (blob.w * parent.width) : (blob.h * parent.height);
				float distance = 0;
				distance = PApplet.sqrt((blob.x * parent.width - parent.width / 2) * (blob.x * parent.width - parent.width / 2) + (blob.y * parent.height - parent.height / 2) * (blob.y * parent.height - parent.height / 2));
				int color = colorSpaces.LCHtoColor(50, 100, 180 - PApplet.map(distance, 0, parent.width / 2, 0, 360), Math.round(PApplet.map(distance, 0, parent.width / 2, 50, 230)));
//				System.out.println("Color Hue: " + (180 - PApplet.map(distance, 0, parent.width / 2, 0, 360)));
				circle = new Circle(parent, new PVector(blob.x * parent.width, blob.y * parent.height), radius, color, System.currentTimeMillis() + 50);
//				circle = new Circle(parent, new PVector(blob.x * parent.width, blob.y * parent.height), parent.height / 10, color, System.currentTimeMillis() + 50);
//				circle.draw();
				if(blob.x * parent.width < 0 || blob.y * parent.height < 0)
				{
					System.out.println("Blob offscreen!");
				}
				if(blobs.size() == maxBlobs)
				{
					blobs.remove(0);
				}
				blobs.add(circle);
//				parent.fill(color);
//				parent.rect(blob.xMin * parent.width, blob.yMin * parent.height, blob.w * parent.width, blob.h * parent.height);
			}
		}
//		drawUnexpiredCircles(blobs);
		parent.fill(oldFill);
	}
	
	private void drawFluid()
	{
//		pg_obstacles = (PGraphics2D) parent.createGraphics(parent.width, parent.height, PApplet.P2D);
		pg_obstacles.beginDraw();
		pg_obstacles.clear();
	    //Boundary circle obstacle
	    float radius = parent.height;
	    pg_obstacles.stroke(parent.color(255));
	    pg_obstacles.strokeWeight(20);
	    pg_obstacles.noFill();
	    pg_obstacles.ellipse(parent.width / 2f, parent.height / 2f, radius - pg_obstacles.strokeWeight, radius - pg_obstacles.strokeWeight);
		if(kinect.getCurrentFrame() != null)
		{
			int oldMode = pg_obstacles.imageMode;
			PImage depthMaskedImage = kinect.getCurrentFrame();
			pg_obstacles.imageMode(PApplet.CENTER);
//			pg_obstacles.image(kinect.getCurrentFrame(), 0, 0, pg_obstacles.width, pg_obstacles.height);
			pg_obstacles.image(depthMaskedImage, pg_obstacles.width / 2, pg_obstacles.height / 2, pg_obstacles.height * 4f / 3f, pg_obstacles.height);
//			pg_obstacles.image(kinect.getCurrentFrame(), pg_obstacles.width / 2, pg_obstacles.height / 2, pg_obstacles.height * 4f / 3f, pg_obstacles.height);
//			pg_obstacles.image(kinect.getCurrentFrame(), 0, 0, kinect.getCurrentFrame().width, kinect.getCurrentFrame().height);
//			pg_obstacles.image(depthMaskedImage, 0, 0, kinect.getCurrentFrame().width, kinect.getCurrentFrame().height);
			pg_obstacles.imageMode(oldMode);
		}
		pg_obstacles.endDraw();
		
		pg_fluid.beginDraw();
	    pg_fluid.background(0);
	    pg_fluid.endDraw();
	    
		// render: density (0), temperature (1), pressure (2), velocity (3)
		fluid.renderFluidTextures(pg_fluid, 0);

		parent.image(pg_fluid, 0, 0);
//		parent.image(pg_obstacles, 0, 0);
		parent.image(pg_obstacles, 0, 0, parent.width / 4, parent.height / 4);
		
//		System.out.println(String.format("[fps %6.2f]", parent.frameRate));
	}
	
	private void resetFluid()
	{
		fluid.reset();
	}
	
	private void drawBokeh()
	{
		FFT fftLog = analysis.getFftLog();
//		float centerFrequency = 0;
		for(int i = 0; i < fftLog.avgSize(); i++)
		{
//			centerFrequency = fftLog.getAverageCenterFrequency(i);
//			float averageWidth = fftLog.getAverageBandWidth(i);
//			float lowFreq = centerFrequency - averageWidth / 2;
//			float highFreq = centerFrequency + averageWidth / 2;
//			int xl = Math.round(fftLog.freqToIndex(lowFreq));
//			int xr = Math.round(fftLog.freqToIndex(highFreq));
//			int xm = Math.round((xl + xr) / 2f);
			float average = fftLog.getAvg(i);
//			float averageScaled = average * spectrumScale;
			parent.fill(255, 128);
//			parent.text("Center Frequency: " + centerFrequency + 
//					", Average Width: " + averageWidth + ", lowFreq: " + 
//					lowFreq + ", highFreq: " + highFreq + ", xl: " + 
//					xl + ", xr: " + xr + ", xm: " + xm + ", average: " + 
//					average + ", averageScaled: " + averageScaled, 10, 10 + 30 * i);
//			System.out.println("Center Frequency: " + centerFrequency + 
//					", Average Width: " + averageWidth + ", lowFreq: " + 
//					lowFreq + ", highFreq: " + highFreq + ", xl: " + 
//					xl + ", xr: " + xr + ", xm: " + xm + ", average: " + 
//					average + ", averageScaled: " + averageScaled);
//			parent.fill(255, 0, 0);
			
			if(average >= threshold)
			{
				circles.add(createCircleFromAverage(i, average, fftLog));
			}
		}
		
		drawUnexpiredCircles(circles);
	}
	
	private Circle createCircleFromAverage(int index, float average, FFT fft)
	{
		float noiseScale = 0.2f * parent.width;
		float noiseX = parent.noise(average) * noiseScale * (random.nextBoolean() ? 1 : -1);
		float noiseY = parent.noise(average) * noiseScale * (random.nextBoolean() ? 1 : -1);
		
		long durationMillis = 5000 - Math.round(PApplet.map(average, 0, 100, 250, 5000));
		
		float radius = PApplet.map(average, 0, 100, 1, parent.height / 4);
		
		PVector origin = new PVector(noiseX + PApplet.map(index, 0, fft.avgSize() - 1, radius, parent.width - radius), noiseY + parent.height - PApplet.map(average, 0, 100, radius, parent.height * 4f / 3f - radius), 0);
		
		int color = colorSpaces.LCHtoColor(50, 100, 180 - PApplet.map(average, 0, 100, 0, 360), Math.round(PApplet.map(average, 0, 100, 50, 255)));
		
		return new Circle(parent, origin, radius, color, System.currentTimeMillis() + durationMillis);
	}
	
	private void drawUnexpiredCircles(ArrayList<Circle> circles)
	{
		for(int i = 0; i < circles.size();)
		{
			Circle circle = circles.get(i);
			if(System.currentTimeMillis() >= circle.getExpirationTimestamp())
			{
				circles.remove(i);
			}
			else
			{
				circle.setRadialCoordinates(isRadialCoordinates);
				circle.draw();
				i++;
			}
		}
//		System.out.println("Circles Size: " + circles.size());
	}
	
	public VisualizationType getVisualizationType()
	{
		return visualizationType;
	}

	public void setVisualizationType(VisualizationType visualizationType)
	{
		this.visualizationType = visualizationType;
	}

	public enum VisualizationType
	{
		BOKEH,
		FLUID
	}

	public boolean isRadialCoordinates()
	{
		return isRadialCoordinates;
	}

	public void setRadialCoordinates(boolean isRadialCoordinates)
	{
		this.isRadialCoordinates = isRadialCoordinates;
	}
	
	public void incrementDissipationDensityCyclic()
	{
		float density = fluid.param.dissipation_density;
	    float velocity = fluid.param.dissipation_velocity;
	    float temperature = fluid.param.dissipation_temperature;
	    float vorticity = fluid.param.vorticity;
	    density = (density + 0.1f < 0.95f) ? density + 0.1f : 0.10f;
		fluid.reset();
		fluid.param.dissipation_density     = density;
	    fluid.param.dissipation_velocity    = velocity;
	    fluid.param.dissipation_temperature = temperature;
	    fluid.param.vorticity               = vorticity;
	}
	
	public float getDissipationDensity()
	{
		return fluid.param.dissipation_density;
	}
	
	public void incrementDissipationVelocityCyclic()
	{
		float density = fluid.param.dissipation_density;
	    float velocity = fluid.param.dissipation_velocity;
	    float temperature = fluid.param.dissipation_temperature;
	    float vorticity = fluid.param.vorticity;
	    velocity = (velocity + 0.1f < 0.95f) ? velocity + 0.1f : 0.10f;
		fluid.reset();
		fluid.param.dissipation_density     = density;
	    fluid.param.dissipation_velocity    = velocity;
	    fluid.param.dissipation_temperature = temperature;
	    fluid.param.vorticity               = vorticity;
	}
	
	public float getDissipationVelocity()
	{
		return fluid.param.dissipation_velocity;
	}
	
	public void incrementDissipationTemperatureCyclic()
	{
		float density = fluid.param.dissipation_density;
	    float velocity = fluid.param.dissipation_velocity;
	    float temperature = fluid.param.dissipation_temperature;
	    float vorticity = fluid.param.vorticity;
	    temperature = (temperature + 0.1f < 0.9f) ? temperature + 0.1f : 0.10f;
		fluid.reset();
		fluid.param.dissipation_density     = density;
	    fluid.param.dissipation_velocity    = velocity;
	    fluid.param.dissipation_temperature = temperature;
	    fluid.param.vorticity               = vorticity;
	}
	
	public float getDissipationTemperature()
	{
		return fluid.param.dissipation_temperature;
	}
}
