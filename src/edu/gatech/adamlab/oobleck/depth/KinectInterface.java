package edu.gatech.adamlab.oobleck.depth;

import org.openkinect.processing.Kinect;

import processing.core.PApplet;
import processing.core.PImage;

public class KinectInterface
{
	private PApplet papplet;
	private Kinect kinect;
	private PImage depthFrame;
	private PImage irFrame;
	
	private boolean isDepth = true;
	private boolean isThresholded = true;
	private float maxThreshold = 750.0f;
	private float minThreshold = 350.0f;
	
	private float[] distancesFromCenter;
	
	public KinectInterface(PApplet papplet)
	{
		this.papplet = papplet;
		isDepth = true;
		try
		{
			kinect = new Kinect(papplet);
			if(kinect != null)
			{
				kinect.initDepth();
				kinect.initVideo();
				kinect.enableIR(true);
			    kinect.enableMirror(true);
			}
			
			firstUpdate();
			distancesFromCenter = new float[getCurrentFrame().width * getCurrentFrame().height];
			initializeDistancesFromCenter();
		}
		catch(Exception e)
		{
//			e.printStackTrace();
			System.out.println("Kinect Not Initialized! Make Sure Kinect Is Plugged In And Configured Correctly!");
		}
	}
	
	private void initializeDistancesFromCenter()
	{
		int frameWidth = getCurrentFrame().width;
		int frameHeight = getCurrentFrame().height;
//		System.out.println("Frame Width: " + frameWidth + ", Frame Height: " + frameHeight + ", Product: " + frameWidth * frameHeight);
		int xcenter = frameWidth / 2;
		int ycenter = frameHeight / 2;
		int x = 0;
		int y = 0;
		for(int i = 0; i < distancesFromCenter.length; i++)
		{
			x = i % frameWidth;
			y = i / frameWidth;
			distancesFromCenter[i] = PApplet.sqrt((x - xcenter) * (x - xcenter) + (y - ycenter) * (y - ycenter)) + 5;
			if(i % 100 == 0)
			{
//				System.out.println("X: " + x + ", Y: " + y + ", Distance: " + distancesFromCenter[i]);
			}
		}
//		System.out.println("XMax: " + x + ", YMax: " + y);
	}
	
	public void firstUpdate()
	{
		if(kinect != null)
		{
			depthFrame = kinect.getDepthImage();
			irFrame = kinect.getVideoImage();
		}
	}
	
	public void update()
	{
		if(kinect != null)
		{
			if(isDepth)
			{
				depthFrame = kinect.getDepthImage();
				if(isThresholded)
				{
					depthFrame = thresholdImage();
				}
			}
			else
			{
				irFrame = kinect.getVideoImage();
				if(isThresholded)
				{
					irFrame = thresholdImage();
				}
			}
		}
	}
	
	private PImage thresholdImage()
	{
		PImage currentFrame = getCurrentFrame();
		int[] depthMap = kinect.getRawDepth();
//		System.out.println("Depth Map Length: " + depthMap.length);
		if(currentFrame == null)
		{
			return null;
		}
		
		PImage newFrame = papplet.createImage(currentFrame.width, currentFrame.height, PApplet.ARGB);
		currentFrame.loadPixels();
		newFrame.loadPixels();
		for(int i = 0; i < depthMap.length; i++)
		{
			if(depthMap[i] <= this.maxThreshold && depthMap[i] >= this.minThreshold && distancesFromCenter[i] < currentFrame.height / 2)
			{
				newFrame.pixels[i] = papplet.color(255, 255);
			}
			else
			{
				newFrame.pixels[i] = papplet.color(0, 0);
			}
		}
		currentFrame.updatePixels();
		newFrame.updatePixels();
		return newFrame;
	}
	
	public void drawImage()
	{
		papplet.background(0);
		if(isDepth)
		{
			if(depthFrame != null)
			{
				papplet.image(depthFrame, 0, 0, papplet.width, papplet.height);
			}
		}
		else
		{
			if(irFrame != null)
			{
				papplet.image(irFrame, 0, 0, papplet.width, papplet.height);
			}
		}
	}
	
	public PImage getDepthFrame()
	{
		return depthFrame;
	}
	
	public PImage getIRFrame()
	{
		return irFrame;
	}
	
	public PImage getCurrentFrame()
	{
		if(isDepth)
		{
			return depthFrame;
		}
		else
		{
			return irFrame;
		}
	}
	
	public boolean isDepth()
	{
		return isDepth;
	}
	
	public void setIsDepth(boolean isDepth)
	{
		this.isDepth = isDepth;
	}
	
	public void toggleIsDepth()
	{
		isDepth = !isDepth;
	}

	public boolean isThresholded()
	{
		return isThresholded;
	}

	public void setThresholded(boolean isThresholded)
	{
		this.isThresholded = isThresholded;
	}

	public float getMaxThreshold()
	{
		return maxThreshold;
	}

	public void setMaxThreshold(float maxThreshold)
	{
		this.maxThreshold = maxThreshold;
	}

	public float getMinThreshold()
	{
		return minThreshold;
	}

	public void setMinThreshold(float minThreshold)
	{
		this.minThreshold = minThreshold;
	}

	public float[] getDistancesFromCenter()
	{
		return distancesFromCenter;
	}

	public void setDistancesFromCenter(float[] distancesFromCenter)
	{
		this.distancesFromCenter = distancesFromCenter;
	}
}
