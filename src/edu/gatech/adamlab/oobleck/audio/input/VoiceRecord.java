package edu.gatech.adamlab.oobleck.audio.input;


import ddf.minim.AudioInput;
import ddf.minim.AudioRecorder;
import ddf.minim.Minim;
import ddf.minim.ugens.*;
import processing.core.PApplet;

public class VoiceRecord {
	
	PApplet parent;
	Minim minim;
	AudioInput in;
	AudioRecorder recorder;
	
	public VoiceRecord(Minim minim, PApplet parent) {		
		this.parent=parent;
		this.minim=minim;
		this.minim.debugOn();
	}
	
	public AudioRecorder getRecorder() {
		return recorder;
	}
	
	public void setLineIn(AudioInput in, String filename)	{
		this.in = in;
		this.recorder=minim.createRecorder(in, filename);
	}
	
	public boolean getRecording() {
		return this.recorder.isRecording();
		
	}
	
	public void startRecording() {
		
		if(!this.recorder.isRecording())
			this.recorder.beginRecord();
	}
	
	public void stopRecording() {		
		if(this.recorder.isRecording())
			this.recorder.endRecord();
	}
	
	public void saveAsFile() {
		this.recorder.save();

	}

}
