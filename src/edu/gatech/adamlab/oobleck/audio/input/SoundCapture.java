package edu.gatech.adamlab.oobleck.audio.input;

import ddf.minim.AudioInput;
import ddf.minim.Minim;
import processing.core.PApplet;

/**
 * Class that takes in audio input through the line-in.
 * @author mikhail.jacob
 *
 */
public class SoundCapture
{
	PApplet parent;
	Minim minim;
	AudioInput in;
	
	/**
	 * Public constructor
	 * @param minim
	 * @param parent
	 */
	public SoundCapture(Minim minim, PApplet parent)
	{
		this.parent = parent;
		this.minim = minim;
		this.minim.debugOn();
		
		// use the getLineIn method of the Minim object to get an AudioInput
		in = minim.getLineIn();
	}
	
	public void draw()
	{
		// draw the waveforms so we can see what we are monitoring
		for (int i = 0; i < in.bufferSize() - 1; i++)
		{
			parent.line(i, 50 + in.left.get(i) * 50, i + 1, 50 + in.left.get(i + 1) * 50);
			parent.line(i, 150 + in.right.get(i) * 50, i + 1, 150 + in.right.get(i + 1) * 50);
		}

		String monitoringState = in.isMonitoring() ? "enabled" : "disabled";
		parent.text("Input monitoring is currently " + monitoringState + ".", 5, 15);
	}

	/**
	 * @return the in
	 */
	public AudioInput getIn()
	{
		return in;
	}

	/**
	 * @param in the in to set
	 */
	public void setIn(AudioInput in)
	{
		this.in = in;
	}
}
