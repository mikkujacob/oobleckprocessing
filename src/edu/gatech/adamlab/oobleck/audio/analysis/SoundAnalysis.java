package edu.gatech.adamlab.oobleck.audio.analysis;

import ddf.minim.AudioInput;
import ddf.minim.Minim;
import ddf.minim.analysis.FFT;
import processing.core.PApplet;
import processing.core.PFont;

/**
 * Class that analyzes audio
 * 
 * @author mikhail.jacob
 *
 */
public class SoundAnalysis
{
	private PApplet parent;
	private Minim minim;
	private AudioInput lineIn;

	private FFT fftLog;
	private float spectrumScale = 50;

	private PFont font;

	/**
	 * Public constructor
	 * 
	 * @param minim
	 * @param parent
	 */
	public SoundAnalysis(Minim minim, PApplet parent)
	{
		this.parent = parent;
		this.minim = minim;
//		this.minim.debugOn();

		parent.rectMode(PApplet.CORNERS);
		font = parent.loadFont("ArialMT-12.vlw");

		parent.textFont(font);
		parent.textSize(18);
	}

	public void draw()
	{
		fftLog.forward(lineIn.mix);

//		drawBands();
	}
	
	private void drawBands()
	{
		float centerFrequency = 0;
		// draw the logarithmic averages
		{
			// since logarithmically spaced averages are not equally spaced
			// we can't precompute the width for all averages
			for (int i = 0; i < fftLog.avgSize(); i++)
			{
				centerFrequency = fftLog.getAverageCenterFrequency(i);
				// how wide is this average in Hz?
				float averageWidth = fftLog.getAverageBandWidth(i);

				// we calculate the lowest and highest frequencies
				// contained in this average using the center frequency
				// and bandwidth of this average.
				float lowFreq = centerFrequency - averageWidth / 2;
				float highFreq = centerFrequency + averageWidth / 2;

				// freqToIndex converts a frequency in Hz to a spectrum band
				// index
				// that can be passed to getBand. in this case, we simply use
				// the
				// index as coordinates for the rectangle we draw to represent
				// the average.
				int xl = (int) fftLog.freqToIndex(lowFreq);
				int xr = (int) fftLog.freqToIndex(highFreq);

				// if the mouse is inside of this average's rectangle
				// print the center frequency and set the fill color to red
				if (parent.mouseX >= 2 * xl && parent.mouseX < 2 * xr)
				{
					parent.fill(255, 128);
					parent.text("Logarithmic Average Center Frequency: "
							+ centerFrequency, 5, parent.height - 25);
					parent.fill(255, 0, 0);
				}
				else
				{
					parent.fill(255);
				}
				// draw a rectangle for each average, multiply the value by
				// spectrumScale so we can see it better
				parent.rect(2 * xl, parent.height, 2 * xr, parent.height - fftLog.getAvg(i) * spectrumScale);
			}
		}
	}

	/**
	 * @return the lineIn
	 */
	public AudioInput getLineIn()
	{
		return lineIn;
	}

	/**
	 * @param lineIn
	 *            the lineIn to set
	 */
	public void setLineIn(AudioInput lineIn)
	{
		this.lineIn = lineIn;

		fftLog = new FFT(this.lineIn.bufferSize(), this.lineIn.sampleRate());

		// calculate averages based on a miminum octave width of 22 Hz
		// split each octave into three bands
		// this should result in 30 averages
		fftLog.logAverages(22, 3);
	}

	/**
	 * @return the fftLog
	 */
	public FFT getFftLog()
	{
		return fftLog;
	}

	/**
	 * @param fftLog the fftLog to set
	 */
	public void setFftLog(FFT fftLog)
	{
		this.fftLog = fftLog;
	}
}
