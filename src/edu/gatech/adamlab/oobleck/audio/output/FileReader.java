
package edu.gatech.adamlab.oobleck.audio.output;

import ddf.minim.Minim;
import ddf.minim.ugens.*;
import processing.core.PApplet;

public class FileReader {
	FilePlayer player;
	Minim minim;
	PApplet parent;
	
	public FileReader(Minim minim, PApplet parent) {
		this.minim=minim;
		this.parent=parent;
	}
}
