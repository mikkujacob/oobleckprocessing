package edu.gatech.adamlab.oobleck.audio.output;

import ddf.minim.AudioOutput;
import ddf.minim.AudioPlayer;
import ddf.minim.Minim;
import processing.core.PApplet;

public class SoundOutput {
	
	Minim minim;
	PApplet parent;
	AudioOutput out;
	AudioPlayer player;
	/**
	 * Public constructor
	 * @param minim
	 * @param parent
	 */
	public SoundOutput(Minim minim, PApplet parent)
	{
		this.parent = parent;
		this.minim = minim;
		this.minim.debugOn();
	}
	
	public void setOutput() {
		this.out=out;
	}
	
	public void getOutput() {
		this.out=minim.getLineOut(Minim.STEREO);
	}
	
	public void readFile(String filename) {
		this.player=minim.loadFile(filename);
	}
	
	public void playFile() {
		this.player.play();
	}
	
	public void pauseFile() {
		this.player.pause();
	}
	
	public AudioPlayer getPlayer() {
		return player;
	}
	

}
