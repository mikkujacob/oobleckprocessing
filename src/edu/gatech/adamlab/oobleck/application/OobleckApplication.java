package edu.gatech.adamlab.oobleck.application;
import processing.core.*;
import ddf.minim.*;
import edu.gatech.adamlab.oobleck.audio.analysis.SoundAnalysis;
import edu.gatech.adamlab.oobleck.audio.input.SoundCapture;
import edu.gatech.adamlab.oobleck.audio.input.VoiceRecord;
import edu.gatech.adamlab.oobleck.audio.output.SoundOutput;
import edu.gatech.adamlab.oobleck.depth.KinectInterface;
import edu.gatech.adamlab.oobleck.visualization.Visualization;
import edu.gatech.adamlab.oobleck.visualization.Visualization.VisualizationType;
//http://www.creativecoding.org/lesson/topics/audio/sound-in-processing
//http://code.compartmental.net/2008/10/12/howto-manipulate-an-audiosample-in-minim-20/
//http://creativec0d3r.blogspot.com/2013/01/how-to-get-frequency-values-from-mic.html
//https://www.ee.columbia.edu/~dpwe/resources/Processing/
/**
 * Application that takes line-in audio input, analyzes it for tone and spectral
 * character, filters it, modifies it procedurally to control an vibrating Oobleck 
 * physical installation piece, and controls a visualization of audio character 
 * projected onto the installation. 
 * @author Damien Di Fede, Anderson Mills, mikhail.jacob, sghosh72, brendancecere
 *
 */
public class OobleckApplication extends PApplet
{
	private Minim minim;
	private SoundCapture capture;
	private SoundAnalysis analysis;
	private SoundOutput out;
	private VoiceRecord recorder;
	private String file_name="myrecording.wav";
	private Visualization visualization;
	private KinectInterface kinectInterface;

	/**
	 * Processing method called before the draw loop starts to setup initial 
	 * application state.
	 */
	public void setup()
	{
		// size(512, 200, P3D);
		// size(displayWidth, displayHeight, P3D);

		minim = new Minim(this);
		capture = new SoundCapture(minim, this);
		analysis = new SoundAnalysis(minim, this);
		analysis.setLineIn(capture.getIn());
		out = new SoundOutput(minim, this);
		recorder = new VoiceRecord(minim, this);
		recorder.setLineIn(capture.getIn(), file_name);
		kinectInterface = new KinectInterface(this);
		visualization = new Visualization(this, analysis, kinectInterface);
	}

	/**
	 * Processing method that is called before set up to initialize settings.
	 */
	public void settings()
	{
		size(displayWidth, displayHeight, P2D);
		fullScreen();
		smooth(2);
	}

	/**
	 * Processing method that is called repeatedly to draw on screen.
	 */
	public void draw()
	{
		background(0);
		stroke(255);
		
//		capture.draw();
		analysis.draw();
		visualization.draw();
	}

	/**
	 * Processing method that handles key press events
	 */
	public void keyPressed()
	{
		if (key == 'm' || key == 'M')
		{
			if (capture.getIn().isMonitoring())
			{
				capture.getIn().disableMonitoring();
			}
			else
			{
				capture.getIn().enableMonitoring();
			}
		}
		
		if (key == 'r' || key == 'R')
		{
			if (recorder.getRecording())
			{
				recorder.stopRecording();
				recorder.saveAsFile();
				out.readFile(file_name);
				out.playFile();
			}
			else
			{
				recorder.startRecording();
			}
		}
		
		if ( key == 's' )
		{
			// we've filled the file out buffer, 
			// now write it to a file of the type we specified in setup
			// in the case of buffered recording, 
			// this will appear to freeze the sketch for sometime, if the buffer is large
			// in the case of streamed recording, 
			// it will not freeze as the data is already in the file and all that is being done
			// is closing the file.
			// save returns the recorded audio in an AudioRecordingStream, 
			// which we can then play with a FilePlayer
//		    if ( player != null )
//		    {
//		        player.unpatch( out );
//		        player.close();
//		    }
//		    player = new FileReader( recorder.save() );
//		    player.patch( out );
//		    player.play();
		}
		
		if(key == '1')
		{
			//Bokeh Radial Visualization
			visualization.setVisualizationType(VisualizationType.BOKEH);
			visualization.setRadialCoordinates(true);
		}
		
		if(key == '2')
		{
			//Bokeh Rectangular Visualization
			visualization.setVisualizationType(VisualizationType.BOKEH);
			visualization.setRadialCoordinates(false);
		}
		
		if(key == '3')
		{
			//Fluid visualization without depth thresholding
			visualization.setVisualizationType(VisualizationType.FLUID);
			kinectInterface.setThresholded(true);
		}
		
		if(key == '4')
		{
			//Fluid visualization with depth thresholding
			visualization.setVisualizationType(VisualizationType.FLUID);
			kinectInterface.setThresholded(false);
		}
		
		if(key == '8')
		{
			//Cyclically increment dissipation density
			visualization.incrementDissipationDensityCyclic();
			System.out.println("Fluid Dissipation Density: " + visualization.getDissipationDensity());
		}
		
		if(key == '9')
		{
			//Cyclically increment dissipation velocity
			visualization.incrementDissipationVelocityCyclic();
			System.out.println("Fluid Dissipation Velocity: " + visualization.getDissipationVelocity());
		}
		
		if(key == '0')
		{
			//Cyclically increment dissipation temperature
			visualization.incrementDissipationTemperatureCyclic();
			System.out.println("Fluid Dissipation Temperature: " + visualization.getDissipationTemperature());
		}
		
		if(key == '-')
		{
			kinectInterface.setMaxThreshold(kinectInterface.getMaxThreshold() - 10f);
			System.out.println("Kinect Max Depth Threshold: " + kinectInterface.getMaxThreshold());
		}
		
		if(key == '=')
		{
			kinectInterface.setMaxThreshold(kinectInterface.getMaxThreshold() + 10f);
			System.out.println("Kinect Max Depth Threshold: " + kinectInterface.getMaxThreshold());
		}
		
		if(key == '_')
		{
			kinectInterface.setMinThreshold(kinectInterface.getMinThreshold() - 10f);
			System.out.println("Kinect Min Depth Threshold: " + kinectInterface.getMinThreshold());
		}
		
		if(key == '+')
		{
			kinectInterface.setMinThreshold(kinectInterface.getMinThreshold() + 10f);
			System.out.println("Kinect Min Depth Threshold: " + kinectInterface.getMinThreshold());
		}
	}

	/**
	 * Public starting point for Java application execution.
	 * @param passedArgs
	 */
	static public void main(String[] passedArgs)
	{
		String[] appletArgs = new String[] { "--present", "--hide-stop", "edu.gatech.adamlab.oobleck.application.OobleckApplication" };
		if (passedArgs != null)
		{
			PApplet.main(concat(appletArgs, passedArgs));
		}
		else
		{
			PApplet.main(appletArgs);
		}
	}
}
